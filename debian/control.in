Source: php-pinba
Section: php
Priority: optional
Maintainer: Debian PHP PECL Maintainers <team+php-pecl@tracker.debian.org>
Uploaders: Vincent Bernat <bernat@debian.org>,
           Prach Pongpanich <prachpub@gmail.com>
Build-Depends: debhelper (>= 10~),
               dh-php (>= 4~),
               libprotobuf-c-dev,
               php-all-dev,
               protobuf-c-compiler
Standards-Version: 4.5.1
Homepage: http://pinba.org
Vcs-Browser: https://salsa.debian.org/php-team/pecl/php-pinba
Vcs-Git: https://salsa.debian.org/php-team/pecl/php-pinba.git
X-PHP-Dummy-Package: yes
X-PHP-Default-Version: 8.2
X-PHP-Versions: 7.0 7.1 7.2 7.3 7.4 8.0 8.1 8.2

Package: php-pinba
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: ${pecl:Provides},
          ${php:Provides}
Breaks: ${pecl:Breaks}
Replaces: ${pecl:Replaces}
Suggests: ${pecl:Suggests}
Description: Pinba module for PHP
 Pinba is a statistics server for PHP using MySQL as a read-only
 interface.
 .
 It accumulates and processes data sent over UDP by multiple PHP
 processes and displays statistics in a nice human-readable form of
 simple "reports", also providing a read-only interface to the raw
 data to enable generation of more sophisticated reports.
 .
 With the Pinba extension, users can also measure particular parts
 of the code using timers with arbitrary tags.
 .
 This package contains a PHP module with the ability to send
 statistics to a Pinba server.
